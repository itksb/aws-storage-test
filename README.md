# Script for testing AWS S3 bucket from PHP 

## Requirements

- Docker 19 and later
- Linux based OS

## Usage

- run ./build.sh to build workspace image
- run ./workspace.sh
- fill credentials for AWS in the /app/.env file 
- composer install
- cd public
- edit index.php
- php index.php - list all files in the bucket
