#!/usr/bin/env bash

echo "running workspace using docker"

if [[ ! -f "app/.env" ]]; then
  echo 'No app/.env file detected. Coping it from the .env.example'
  cp "app/.env.example" "app/.env"
fi

eval $(ssh-agent) \
  docker run --rm --interactive --tty \
  --volume $PWD/app:/app \
  --volume $SSH_AUTH_SOCK:/ssh-auth.sock \
  --volume /etc/passwd:/etc/passwd:ro \
  --volume /etc/group:/etc/group:ro \
  --env SSH_AUTH_SOCK=/ssh-auth.sock \
  --env-file ./app/.env \
  -w /app \
  --user $(id -u):$(id -g) \
  awscli-test bash
