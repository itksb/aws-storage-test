<?php

$DS = DIRECTORY_SEPARATOR;
$APP_DIR = realpath(__DIR__ . "$DS..$DS");
$VENDOR_DIR = realpath($APP_DIR . $DS . 'vendor');

require "$VENDOR_DIR/autoload.php";

$s3 = new Aws\S3\S3Client([
    'version' => 'latest',
    'region' => getenv('AWS_REGION'),
    'credentials' => array(
        'key' => getenv('AWS_KEY'),
        'secret' => getenv('AWS_SECRET')
    )
]);
$objects = $s3->ListObjects([
    'Bucket' => getenv('AWS_BUCKET'),
    'delimiter' => '/',
]);

var_dump ($objects->toArray());
